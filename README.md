# Sisprocesso
API RestFull criada para resolve o desafio proposto pela empresa Stefanini.

## Tecnologia Utilizada:

IDE --> Spring Tool Suite

Linguagem de programação --> JAVA version 8

Framework --> Spring Boot version 2.4.3

JPA --> Java Persistence API

hibernate validator --> version 6.0.13.Final

GIT --> Sistema de controle de versão

GITLAB --> Serviço de Hospedagem de código-fonte

Oracle Database Express Edition 11g --> Sistema de gerenciamento de Banco de Dados

DBeaver --> Modelagem e relacionamento Banco de dados

Postman -- > Para fazer teste das requisições dos endpoints HTTP

## Função

A finalidade do crud é resolve o  desafio proposto pela empresa Stefanini.

### endpoints;

buscarPorId --> Retorna um processo através do id http://localhost:8090/processos/61

insert --> Salva um novo processo no Banco de dados http://localhost:8090/processos/salvar

listar --> Lista todos os processos que estão no Banco de dados http://localhost:8090/processos/lista

atualizarProcesso --> Atualiza um processo http://localhost:8090/processos/update/61

delete --> deleta um processo do Banco de dados http://localhost:8090/processos/delete/46

## Teste do projeto

Basta baixa ou clona e importa na IDE Spring Tool Suite como projeto maven








