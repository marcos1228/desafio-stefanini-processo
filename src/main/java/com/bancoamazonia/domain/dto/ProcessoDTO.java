package com.bancoamazonia.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


import org.hibernate.validator.constraints.Length;

import com.bancoamazonia.domain.model.Processo;
import com.fasterxml.jackson.annotation.JsonFormat;

public class ProcessoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	@NotBlank(message = "Preenchimento obrigatório")
	@Length (min = 19, max = 19, message = "O tamanho tem que se igual a 19")
	private String numeroProcesso;
	@NotNull(message = "Data Cadastro não pode ser vazia")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date dataCadastro;
	@NotNull(message = "Data Cadastro não pode ser vazia")
	private Integer partes;
	@NotBlank(message = "Preenchimento obrigatório")
	private String situacao;
	@NotBlank(message = "Preenchimento obrigatório")
	private String segredo;

	public ProcessoDTO() {
		super();
	}

	public ProcessoDTO(Long id, String numeroProcesso, Date dataCadastro, Integer partes, String situacao,
			String segredo) {
		super();
		this.id = id;
		this.numeroProcesso = numeroProcesso;
		this.dataCadastro = dataCadastro;
		this.partes = partes;
		this.situacao = situacao;
		this.segredo = segredo;
	}

	public ProcessoDTO(Processo processo) {
		this.id = processo.getId();
		this.numeroProcesso = processo.getNumeroProcesso();
		this.dataCadastro = processo.getDataCadastro();
		this.partes = processo.getPartes();
		this.situacao = processo.getSituacao().toString();
		this.segredo = processo.getSegredo().toString();

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getPartes() {
		return partes;
	}

	public void setPartes(Integer partes) {
		this.partes = partes;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getSegredo() {
		return segredo;
	}

	public void setSegredo(String segredo) {
		this.segredo = segredo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcessoDTO other = (ProcessoDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
