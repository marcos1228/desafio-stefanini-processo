package com.bancoamazonia.domain.enums;

public enum SegredoJusticaEnum {
	PUBLICO, PRIVADO;

	public static SegredoJusticaEnum find(String value) {
		for (SegredoJusticaEnum segredo : SegredoJusticaEnum.values()) {
			if (segredo.name().equalsIgnoreCase(value))
				return segredo;
		}
		throw new IllegalArgumentException("Segredo de Justiça Inválido");
	}
}
