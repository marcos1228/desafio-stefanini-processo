package com.bancoamazonia.domain.enums;

public enum SituacaoEnum {
	EMANDAMENTO, JULGADO, ARQUIVADO;

	public static SituacaoEnum find(String value) {
		for (SituacaoEnum situacao : SituacaoEnum.values()) {
			if (situacao.name().equalsIgnoreCase(value))
				return situacao;
		}
		throw new IllegalArgumentException("Situação Inválida");
	}

}
