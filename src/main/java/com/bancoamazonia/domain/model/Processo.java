package com.bancoamazonia.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bancoamazonia.domain.enums.SegredoJusticaEnum;
import com.bancoamazonia.domain.enums.SituacaoEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "CAD_PROCESSO")
public class Processo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	@Column(name = "NU_PROCESSO")
	private String numeroProcesso;
	@Column(name = "DH_CADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataCadastro;
	@Column(name = "NU_PARTES")
	private Integer partes;
	@Column(name = "EN_SITUACAO", nullable = false)
	@Enumerated(EnumType.STRING)
	private SituacaoEnum situacao;
	@Column(name = "EN_SEGREDO_DE_JUSTICA", nullable = false)
	@Enumerated(EnumType.STRING)
	private SegredoJusticaEnum segredo;

	public Processo() {
		super();
	}

	public Processo(Long id, String numeroProcesso, Date dataCadastro, Integer partes, String situacao,
			String segredo) {
		super();
		this.id = id;
		this.numeroProcesso = numeroProcesso;
		this.dataCadastro = dataCadastro;
		this.partes = partes;
		this.situacao = SituacaoEnum.find(situacao);
		this.segredo = SegredoJusticaEnum.find(segredo);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getPartes() {
		return partes;
	}

	public void setPartes(Integer partes) {
		this.partes = partes;
	}

	public SituacaoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

	public SegredoJusticaEnum getSegredo() {
		return segredo;
	}

	public void setSegredo(SegredoJusticaEnum segredo) {
		this.segredo = segredo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Processo other = (Processo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
