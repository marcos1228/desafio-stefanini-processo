package com.bancoamazonia.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bancoamazonia.domain.model.Processo;

@Repository
public interface ProcessoRepository extends JpaRepository<Processo, Long> {
 Optional<Processo> findById(Long id);
 
 
}
