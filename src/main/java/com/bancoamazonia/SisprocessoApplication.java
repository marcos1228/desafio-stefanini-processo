package com.bancoamazonia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SisprocessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SisprocessoApplication.class, args);
	}

}
