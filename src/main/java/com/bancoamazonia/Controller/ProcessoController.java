package com.bancoamazonia.Controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bancoamazonia.Manager.ProcessoManager;
import com.bancoamazonia.domain.dto.ProcessoDTO;
import com.bancoamazonia.domain.model.Processo;
/**
 @author Marcos Barbosa Evangelista
 @version 1
 **/
@RestController
@RequestMapping(value = "/processos")
public class ProcessoController {

	@Autowired
	private ProcessoManager processoManager;

	@GetMapping(value = "{id}")
	public ResponseEntity<Processo> buscarPorId(@PathVariable Long id) {
		Processo processo = processoManager.buscarPorId(id);
		return ResponseEntity.ok().body(processo);
	}

	@PostMapping(value = "/salvar")
	public ResponseEntity<?> insert(@Valid @RequestBody ProcessoDTO processoDto) {
		processoDto.setId(null);
		Processo obj = processoManager.fromDTO(processoDto);
		obj = processoManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@GetMapping(value = "/lista")
	public ResponseEntity<List<ProcessoDTO>> listar() {
		return ResponseEntity.ok(processoManager.listarProcesso());
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> atualizarProcesso( @RequestBody ProcessoDTO processoDto, @PathVariable Long id) {
		processoDto.setId(id);
		processoManager.update(processoDto);		
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		processoManager.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}
