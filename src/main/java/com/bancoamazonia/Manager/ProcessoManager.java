package com.bancoamazonia.Manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bancoamazonia.Manager.exception.OjectNotFoundException;
import com.bancoamazonia.domain.dto.ProcessoDTO;
import com.bancoamazonia.domain.enums.SegredoJusticaEnum;
import com.bancoamazonia.domain.enums.SituacaoEnum;
import com.bancoamazonia.domain.model.Processo;
import com.bancoamazonia.repositories.ProcessoRepository;

@Component
public class ProcessoManager {

	@Autowired
	private ProcessoRepository processoRepository;

	public Processo insert(Processo processo) {
		return processoRepository.save(processo);
	}

	public Processo fromDTO(ProcessoDTO processoDTO) {
		Processo processo = new Processo(null, processoDTO.getNumeroProcesso(), processoDTO.getDataCadastro(),
				processoDTO.getPartes(), processoDTO.getSituacao(), processoDTO.getSegredo());
		return processo;
	}

	public List<ProcessoDTO> listarProcesso() {
		List<Processo> processos = processoRepository.findAll();
		List<ProcessoDTO> list = new ArrayList<>();
		for (Processo p : processos) {
			list.add(new ProcessoDTO(p));
		}
		return list;
	}

	public Processo buscarPorId(Long id) {
		Optional<Processo> processo = processoRepository.findById(id);

		return processo.orElseThrow(
				() -> new OjectNotFoundException("Processo não encontrado! Id: " + id + ",Tipo" + Processo.class));
	}

	public ProcessoDTO update(ProcessoDTO processoDto) {
		Optional<Processo> optional = processoRepository.findById(processoDto.getId());
		if (optional.isPresent()) {

			Processo processo = optional.get();
			processo.setNumeroProcesso(processoDto.getNumeroProcesso());
			processo.setDataCadastro(processoDto.getDataCadastro());
			processo.setPartes(processoDto.getPartes());
			processo.setSituacao(SituacaoEnum.find(processoDto.getSituacao()));
			processo.setSegredo(SegredoJusticaEnum.find(processoDto.getSegredo()));
			processoRepository.save(processo);
		}
		return new ProcessoDTO();
	}

	public void delete(Long id) {
		buscarPorId(id);
		processoRepository.deleteById(id);
	}
}